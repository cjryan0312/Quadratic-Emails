import os

import imaplib

import email

import html2text

from quadratic_functions import standard_to_vertex, num_real_answers, \
    find_answers, find_unfactored, find_factored

run = 1
body = ''
sender = ''
content_type = "text/plain"
message = ''

# credentials
EMAIL_ADDRESS = os.environ.get('EMAIL_ADDRESS')
EMAIL_PASSWORD = os.environ.get('EMAIL_PASSWORD')

# connect to google IMAP server and enter email credentials
imap = imaplib.IMAP4_SSL("imap.gmail.com")
imap.login(EMAIL_ADDRESS, EMAIL_PASSWORD)

status, messages = imap.select("INBOX", readonly=False)
# number of emails retrieved
N = 1
# total number of emails in the inbox
messages = int(messages[0])
# if no messages, the running.py will not use send_email.py
if messages == 0:
    run = 0


# delete email function
def deleteEmail():
    imap.store(str(messages), '+FLAGS', '\\Deleted')
    imap.expunge()


if messages != 0:
    for i in range(messages, messages - N, -1):
        res, msg = imap.fetch(str(i), "(RFC822)")
        for response in msg:
            if isinstance(response, tuple):
                msg = email.message_from_bytes(response[1])
                sender = msg.get("From")
                if msg.is_multipart():
                    for part in msg.walk():
                        content_type = part.get_content_type()
                        content_disposition = str(part.get("Content-Disposition"))
                        try:
                            body = str(part.get_payload(decode=True).decode())
                        except:
                            pass
                        if content_type == "text/plain" and "attachment" not in content_disposition:
                            pass
                        elif "attachment" in content_disposition:
                            pass
                else:
                    content_type = msg.get_content_type()
                    body = str(msg.get_payload(decode=True).decode())
                    if content_type == "text/plain":
                        pass
                if content_type == "text/html":
                    body = html2text.html2text(body)

if "<" in sender:
    symbol1 = sender.find('<')
    symbol2 = sender.find('>')
    sender = sender[symbol1 + 1:symbol2]

myList = []
myStr = ''
for item in body:
    if item == '\r' or item == '\n':
        myList.append(myStr)
        myStr = ''
        while '' in myList:
            myList.remove('')
        continue
    myStr += item


def sending_message():
    if run == 0:
        message = 'it seems there are 0 emails'
        return message

    try:
        a = int(myList[0])
        b = int(myList[1])
        c = int(myList[2])
    except:
        message = "You have input an invalid character, please try again."
        deleteEmail()
        return message

    quad_disc = b ** 2 - 4 * a * c
    if a == 0:
        lin_equation = f"y = {b}x + {c}"
        message = f"a is 0 so it's a linear function with an equation of {lin_equation}"
        deleteEmail()
        return message

    elif "One" not in num_real_answers(quad_disc):
        message = f"""
        {num_real_answers(quad_disc)}
        The vertex form of the equation is: {standard_to_vertex(a, b, c)}
        The answers are: {find_answers(quad_disc, a, b)}
        The unfactored equation is: {find_unfactored(quad_disc, a, b)}
        The factored equation is: {find_factored(quad_disc, a, b)}
        """
        deleteEmail()
        return message

    else:
        message = f"""
        The vertex form of the equation is: {standard_to_vertex(a, b, c)}
        The answer is: {find_answers(quad_disc, a, b)}
        The unfactored equation is: {find_unfactored(quad_disc, a, b)}
        The factored equation is: {find_factored(quad_disc, a, b)}
        """
        deleteEmail()
        return message


message = sending_message()
imap.close()
imap.logout()
