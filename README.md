# About:
Program that reads emails sent to <quadraticbutterpy@gmail.com> that have the A, B, C of the quadratic formula, solves it using code from the Quadratic repo found [here](https://gitlab.com/cjryan0312/Quadratic), and sends an email back to the original address.

# How To Use:
Format your email like this:

![format](https://media.discordapp.net/attachments/640193789532569611/750768096347947148/unknown.png)

And you will get an email back like this:

![format](https://media.discordapp.net/attachments/570785781325758475/761643599754166363/unknown.png)
