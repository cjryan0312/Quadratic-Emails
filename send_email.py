import smtplib

import os

from email.message import EmailMessage

from read_email import message, sender, run

port = 465
smtp_server = "smtp.gmail.com"
EMAIL_ADDRESS = os.environ.get('EMAIL_ADDRESS')
EMAIL_PASSWORD = os.environ.get('EMAIL_PASSWORD')


def send_email():
    msg = EmailMessage()
    msg['Subject'] = "Quadratic Formula Solution"
    msg['From'] = EMAIL_ADDRESS
    msg['To'] = sender
    msg.set_content(message)

    with smtplib.SMTP_SSL(smtp_server, port) as smtp:
        smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        smtp.send_message(msg)


if run != 0:
    send_email()
    print("Email sent!")
else:
    print("No emails found")
